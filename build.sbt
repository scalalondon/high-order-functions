name := """higher-order-functions"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies ++= {
  val scalaTestVersion = "2.2.4"
  Seq(
  "com.github.fge" % "json-schema-validator" % "2.2.6",
    "org.scalatest" %% "scalatest" % scalaTestVersion % "test")
}
// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

