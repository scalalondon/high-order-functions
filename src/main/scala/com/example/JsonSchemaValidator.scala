package com.example

import java.net.URL

import com.fasterxml.jackson.databind.JsonNode
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.report.ProcessingReport
import com.github.fge.jsonschema.main.{JsonSchemaFactory, JsonSchema}

/**
 * Contain JSON Schema Validator.
 */
trait JsonSchemaValidator {

  val factory: JsonSchemaFactory = JsonSchemaFactory.byDefault()

  /**
   * Strict Validator for JSON String.
   *
   * @param jsonMsg JSON as a String
   * @param jsonSchema implicit jsonSchema to use for validation
   * @return true if jsonMsg passes json schema validation
   */
  def strictJsonValidator(jsonMsg: String)(implicit jsonSchema: JsonSchema): Boolean = {
    val jsonNode: JsonNode = JsonLoader.fromString(jsonMsg)
    val processingReport: ProcessingReport = jsonSchema.validate(jsonNode)
    processingReport.isSuccess
  }

  /**
   * Simply logs validation issues, returns true for valid and invalid JSON
   *
   * @param jsonMsg JSON as a String
   * @param jsonSchema implicit jsonSchema to use for validation
   * @return true
   */
  def warningJsonValidator(jsonMsg: String)(implicit jsonSchema: JsonSchema): Boolean = {
    val jsonNode: JsonNode = JsonLoader.fromString(jsonMsg)
    val processingReport: ProcessingReport = jsonSchema.validate(jsonNode)
    if(processingReport.isSuccess) println("Validation Succeeded") else println("Validation Failed")
    true
  }

  def jsonSchema(resourcePath: String): JsonSchema = {
    val url: URL = getClass.getResource(resourcePath)
    factory.getJsonSchema("file://"+url.getPath)
  }

  def jsonMsg(resourcePath: String): String = {
    val jsonNode: JsonNode = JsonLoader.fromResource(resourcePath)
    jsonNode.toString
  }

}
