package com.example

import java.io.File
import java.net.{URL, URI}

import com.fasterxml.jackson.databind.JsonNode
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.report.ProcessingReport
import com.github.fge.jsonschema.main.{JsonSchema, JsonSchemaFactory}

import scala.concurrent.Future

/**
 * FlatAd Message Processor.
 */
class FlatAdMessageConsumer extends MessageConsumer with JsonSchemaValidator {

  implicit val flatAdJsonSchema: JsonSchema = jsonSchema("/example-schema.json")
  
  def process(body: String): Future[Boolean] = {

    def processor(body: String): Boolean = {
      println(s"Sending $body to ElasticSearch")
      true
    }

    process(body, strictJsonValidator, processor)

  }

}
