package com.example

/**
 * Model.
 */
case class FlatAd(id: Int, version: Int, title: String, description: String)
