package com.example

import scala.concurrent.Future

/**
 * Message Consumer.
 */
trait MessageConsumer {

  /**
   * Each consumer needs to implement this method
   *
   * @param body
   * @return
   */
  def process(body: String): Future[Boolean]

  /**
   * Take a JSON String validates and process it.
   *
   * @param msg message to process
   * @param validator validator to verify msg content
   * @param processor how to process the message
   * @return true if msg was processed successfully
   */
  def process(msg: String, validator: (String) => Boolean, processor: (String) => Boolean): Future[Boolean] = {

    import scala.concurrent.ExecutionContext.Implicits.global
    if(validator(msg)) {
      Future {
        processor(msg)
      }
    } else { Future.failed(new MessageValidationException) }

  }

}

class MessageValidationException extends RuntimeException