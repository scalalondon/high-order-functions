package com.example

import org.scalatest.concurrent.{ScalaFutures, Futures}
import org.scalatest.time.{Seconds, Span, Millis}
import org.scalatest.{Matchers, GivenWhenThen, FeatureSpec}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

/**
 * Created by tony_murphy on 23/10/2015.
 */
class FlatAdMessageConsumerSpec extends FeatureSpec
  with Matchers with GivenWhenThen with JsonSchemaValidator with ScalaFutures {


  implicit val defaultPatience =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  feature("FlatAd Message Consumer") {

    info("Additional Information Here")

    scenario("When A FlatAd Message is consumed via Camel") {

      Given("Message Body Starts with 'a'")

      When("When it's processed")

      val body: String = jsonMsg("/example.json")

      val f: Future[Boolean] = (new FlatAdMessageConsumer()).process(body)

      Then("It should be true")
      Await.result(f,1 second) shouldBe true

    }

    scenario("Message starting with letter other than a") {

      Given("Message Body Starts with 'b'")
      val body: String = jsonMsg("/bad.json")

      When("When it's processed")
      val f: Future[Boolean] = (new FlatAdMessageConsumer()).process(body)
      Then("It should be true")


      Then("It should be true")

      whenReady(f.failed) { e =>
        e shouldBe a [MessageValidationException]
      }
    }
  }

}
